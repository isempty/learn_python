#! d:/soft/python/python37/python

# cleanup.py

f = None

try:
    f = open('data.txt', 'r')
    contents = f.readlines()

    for line in contents:
        print(line.rstrip())

except IOError:
    print('Error opening file')

finally:
    if f:
        f.close()
