#! d:/soft/python/python37/python

# exception_argument.py

try:
    a = (1, 2, 3, 4)
    print(a[5])

except IndexError as e:
    print(e)
    print("Class:", e.__class__)


