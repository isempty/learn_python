#! d:/soft/python/python37/python

# fahrenheit.py

def c2f(c):
    return c*9/5 + 32

print(c2f(100))
print(c2f(0))
print(c2f(30))
