#! d:/soft/python/python37/python

# file_positions.py

import sys

with open('works.txt','r') as f:
    print(f.read(14))
    print("The current file position is {0}".format(f.tell()))

    f.seek(0,0)

    print(f.read(30))
