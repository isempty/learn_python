#! d:/soft/python/python37/python

# iterator.py

str = "formidable"

for e in str:
    print(e, end=" ")

print()

it = iter(str)

print(next(it))
print(next(it))
print(next(it))

print(list(it))
