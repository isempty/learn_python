#!/usr/bin/python3

# removing.py

langs = ["Python","Ruby","Perl","Lua","Javascript"]
print(langs)

lang = langs.pop(3)
print("{0} was removed".format(lang))

lang = langs.pop()
print("{0} was removed".format(lang))

print(langs)

langs.remove("Ruby")
print(langs)
