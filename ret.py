#! d:/soft/python/python37/python

"""
The ret.py script shows how to work with
functions in Python.
Author:zero
2019.9

"""

def showModuleName():
    print(__doc__)

def getModuleFile():
    return __file__

a = showModuleName()
b = getModuleFile()

print(a,b)

