#!/usr/bin/python3

# various_types.py

class Being:
    pass

objects = [1, -2, 3.4, None, False, [1,2], "Python", (2,3), Being(), {}]
print(objects)
