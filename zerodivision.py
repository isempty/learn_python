#! d:/soft/python/python37/python

# zerodivision.py

def input_numbers():

    a = float(input("Enter first number:"))
    b = float(input("Enter second number:"))
    return a, b

x, y = input_numbers()
print("{0} / {1} is {2}".format(x, y, x/y))
